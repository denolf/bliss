namespace WindowSystemInterface
{
  int _sendWindowSystemEvents(int);
  int _windowSystemEventsQueued();
  int _globalPostedEventsCount();
}
