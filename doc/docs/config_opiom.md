# Opiom configuration


## Yaml sample configuration

```YAML
- class: Opiom
  name: opiom_eh3
  serial:
    url: tango://id13/Serial_133_232/02
    timeout: 30
  program: /users/blissadm/local/isg/opiom/20180625_152307_multiplexer-eh3
```
