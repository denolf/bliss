The *Scan engine* is build around several object which has defined roles.

  * [Scan](scan_engine_scan.md) top level object
  * [Scan saving](gs_presentation.md#scan-saving) file management.
  * [Acquisition chain](scan_engine_acquisition_chain.md) define trigger sequencing
  and master slave dependencies
  * [Acquisition master/devices](scan_engine_acquisition_master_and_devices.md)
  define device trigger and data biaviour of base devices.
  * [Preset](scan_engine_preset.md) hook to control scan encironnement.

## Schema

![Mind](img/scan_engine.png)
