# Standard Beamviewer (EBV: ESRF Beam Viewer)

Configuration and composition of an EBV is described
here: [Beamviewer Configuration](config_beamviewer.md).

## Usage

### Action methods
#### `set_exposure_time()`
#### `set_diode_range()`
#### `live()`
#### `stop()`
#### `set_in()`
#### `set_out()`
#### `save_images()`

### Status methods
#### `is_acquiring()`
#### `is_live()`
#### `is_video_live()`
#### `is_in()`
#### `is_out()`

### Properties
#### `x`
#### `y`
#### `intensity`
#### `fwhm_x`
#### `fwhm_y`
#### `diode_range`
#### `tango_proxy`
#### `image`
#### `exposure_time`
#### `diode_range`
