# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2019 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""Listen the scan data and display in a selected ptpython buffer console """

import sys
import time
import datetime
import numpy
import operator
import termios
import shutil
import signal
import subprocess
import atexit
import contextlib
import gevent
from functools import wraps
from gevent.threadpool import ThreadPool

from bliss.data.scan import watch_session_scans

from bliss import setup_globals
from bliss.common.axis import Axis
from bliss.common.event import dispatcher
from bliss.config.settings import HashSetting
from bliss.scanning.scan import set_scan_watch_callbacks
from bliss.scanning.scan import ScanDisplay
from bliss import global_map
from bliss.scanning.chain import ChainPreset, ChainIterationPreset


if sys.platform not in ["win32", "cygwin"]:
    from blessings import Terminal
else:

    class Terminal:
        def __getattr__(self, prop):
            if prop.startswith("__"):
                raise AttributeError(prop)
            return ""


def catch_sigint(*args):
    pass


def print_full_line(msg, deco="=", head="\n", tail="\n"):
    width = shutil.get_terminal_size().columns
    fac = (width - len(msg)) // 2
    deco = deco * fac

    print("".join([head, deco, msg, deco, tail]))


def _find_obj(name):
    return operator.attrgetter(name)(setup_globals)


def _find_unit(obj):
    try:
        if isinstance(obj, str):
            # in the form obj.x.y
            obj = _find_obj(obj)
        if hasattr(obj, "unit"):
            return obj.unit
        if hasattr(obj, "config"):
            return obj.config.get("unit")
        if hasattr(obj, "controller"):
            return _find_unit(obj.controller)
    except:
        return


def _post_in_pool(func):
    # post in a thread to avoid blocking call due to print function
    @wraps(func)
    def f(self, *args):
        task = self._pool.spawn(func, self, *args)
        return task.get()

    return f


class ScanPrinter:
    """compose scan output"""

    HEADER = (
        "Total {npoints} points{estimation_str}\n"
        + "{not_shown_counters_str}\n"
        + "Scan {scan_nb} {start_time_str} {filename} "
        + "{session_name} user = {user_name}\n"
        + "{title}\n\n"
        + "{column_header}"
    )

    DEFAULT_WIDTH = 12

    def __init__(self):
        self.real_motors = []
        set_scan_watch_callbacks(self.on_scan_new, self.on_scan_data, self.on_scan_end)

    def on_scan_new(self, scan, scan_info):
        scan_type = scan_info.get("type")
        if scan_type is None:
            return
        scan_info = dict(scan_info)
        self.term = Terminal(scan_info.get("stream"))
        nb_points = scan_info.get("npoints")
        if nb_points is None:
            return

        # print("acquisition_chain",scan_info["acquisition_chain"])

        self.col_labels = ["#"]
        self.real_motors = []
        self.counter_names = []
        self.counter_fullnames = []
        self._point_nb = 0
        motor_labels = []
        self.motor_fullnames = []

        master, channels = next(iter(scan_info["acquisition_chain"].items()))

        for channel_fullname in channels["master"]["scalars"]:
            channel_short_name = channels["master"]["display_names"][channel_fullname]
            channel_unit = channels["master"]["scalars_units"][channel_fullname]

            if channel_fullname == "timer:epoch":
                continue

            if channel_short_name == "elapsed_time":
                # timescan
                self.col_labels.insert(1, f"dt[{channel_unit}]")
            else:
                # we can suppose channel_fullname to be a motor name
                try:
                    motor = _find_obj(channel_short_name)
                except Exception:
                    continue
                else:
                    if isinstance(motor, Axis):
                        self.real_motors.append(motor)
                        if self.term.is_a_tty:
                            dispatcher.connect(
                                self._on_motor_position_changed,
                                signal="position",
                                sender=motor,
                            )
                        unit = motor.config.get("unit", default=None)
                        motor_label = global_map.alias_or_name(motor)
                        if unit:
                            motor_label += "[{0}]".format(unit)
                        motor_labels.append(motor_label)
                        self.motor_fullnames.append("axis:" + motor.name)

        for channel_fullname in channels["scalars"]:
            channel_short_name = channels["display_names"][channel_fullname]
            channel_unit = channels["scalars_units"][channel_fullname]

            if channel_short_name == "elapsed_time":
                self.col_labels.insert(1, "dt[s]")
                continue

            if channel_fullname == "timer:epoch":
                continue

            self.counter_names.append(
                channel_short_name + (f"[{channel_unit}]" if channel_unit else "")
            )
            self.counter_fullnames.append(channel_fullname)

        self.col_labels.extend(motor_labels)
        self.col_labels.extend(self.counter_names)

        other_channels = [
            channels["display_names"][channel_fullname]
            for channel_fullname in channels["spectra"] + channels["images"]
        ]
        if other_channels:
            not_shown_counters_str = "Activated counters not shown: %s\n" % ", ".join(
                other_channels
            )
        else:
            not_shown_counters_str = ""

        if scan_type == "ct":
            header = not_shown_counters_str
        else:
            estimation = scan_info.get("estimation")
            if estimation:
                total = datetime.timedelta(seconds=estimation["total_time"])
                motion = datetime.timedelta(seconds=estimation["total_motion_time"])
                count = datetime.timedelta(seconds=estimation["total_count_time"])
                estimation_str = ", {0} (motion: {1}, count: {2})".format(
                    total, motion, count
                )
            else:
                estimation_str = ""

            col_lens = [max(len(x), self.DEFAULT_WIDTH) for x in self.col_labels]
            h_templ = ["{{0:>{width}}}".format(width=col_len) for col_len in col_lens]
            header = "  ".join(
                [templ.format(label) for templ, label in zip(h_templ, self.col_labels)]
            )
            header = self.HEADER.format(
                column_header=header,
                estimation_str=estimation_str,
                not_shown_counters_str=not_shown_counters_str,
                **scan_info,
            )
            self.col_templ = [
                "{{0: >{width}g}}".format(width=col_len) for col_len in col_lens
            ]
        print(header)

    def on_scan_data_ct(self, scan_info, values):
        scan_type = scan_info.get("type")
        if scan_type == "ct":
            # ct is actually a timescan(npoints=1).
            master, channels = next(iter(scan_info["acquisition_chain"].items()))
            count_time = scan_info["count_time"]
            col_len = 0
            cnt_label_values = {}
            for channel_fullname in channels["scalars"]:
                label = channels["display_names"][channel_fullname]
                channel_unit = channels["scalars_units"][channel_fullname]
                col_len = max(len(label), col_len)
                value = values[channel_fullname]
                try:
                    norm_value = value / count_time
                except ZeroDivisionError:
                    norm_value = None
                cnt_label_values[label] = (value, norm_value)

            template = "{{label:>{0}}} = {{value: >12}} ({{norm: 12}}/s)".format(
                col_len
            )
            template_no_norm = "{{label:>{0}}} = {{value: >12}}".format(col_len)
            end_time_str = datetime.datetime.now().strftime("%a %b %d %H:%M:%S %Y")
            print(f"{end_time_str}\n\n")
            for label, (value, norm_value) in cnt_label_values.items():
                temp = template if norm_value else template_no_norm
                print(temp.format(label=label, value=value, norm=norm_value))
            return True

    def on_scan_data(self, scan_info, values):
        scan_type = scan_info.get("type")
        if scan_type is None:
            return

        master, channels = next(iter(scan_info["acquisition_chain"].items()))

        elapsed_time_col = []
        if "timer:elapsed_time" in values:
            elapsed_time_col.append(values.pop("timer:elapsed_time"))

        if not self.on_scan_data_ct(scan_info, values):
            motor_values = [values[motor_name] for motor_name in self.motor_fullnames]
            counter_values = [
                values[counter_fullname] for counter_fullname in self.counter_fullnames
            ]

            values = elapsed_time_col + motor_values + counter_values
            values.insert(0, self._point_nb)
            self._point_nb += 1
            line = "  ".join(
                [self.col_templ[i].format(v) for i, v in enumerate(values)]
            )
            if self.term.is_a_tty:
                monitor = scan_info.get("output_mode", "tail") == "monitor"
                print("\r" + line, end=monitor and "\r" or "\n")
            else:
                print(line)

    def on_scan_end(self, scan_info):
        scan_type = scan_info.get("type")
        if scan_type is None or scan_type == "ct":
            return

        for motor in self.real_motors:
            dispatcher.disconnect(
                self._on_motor_position_changed, signal="position", sender=motor
            )

        end = datetime.datetime.fromtimestamp(time.time())
        start = datetime.datetime.fromtimestamp(scan_info["start_timestamp"])
        dt = end - start
        if scan_info.get("output_mode", "tail") == "monitor" and self.term.is_a_tty:
            print()
        msg = "\nTook {0}".format(dt)
        if "estimation" in scan_info:
            time_estimation = scan_info["estimation"]["total_time"]
            msg += " (estimation was for {0})".format(
                datetime.timedelta(seconds=time_estimation)
            )
        print(msg)

    def _on_motor_position_changed(self, position, signal=None, sender=None):
        labels = []
        for motor in self.real_motors:
            position = "{0:.03f}".format(motor.position)
            unit = motor.config.get("unit", default=None)
            if unit:
                position += "[{0}]".format(unit)
            labels.append("{0}: {1}".format(motor.name, position))

        print("\33[2K", end="")
        print(*labels, sep=", ", end="\r")


class ScanDataListener:

    HEADER = (
        "Total {npoints} points{estimation_str}\n"
        + "{not_shown_counters_str}\n"
        + "Scan {scan_nb} {start_time_str} {filename} "
        + "{session_name} user = {user_name}\n"
        + "{title}\n\n"
        + "{column_header}"
    )

    DEFAULT_WIDTH = 12

    def __init__(self, session_name="", exit_read_fd=None):

        self.session_name = session_name
        self.scan_name = None
        self.scan_is_running = None
        self.counter_selection = []
        self.exit_read_fd = exit_read_fd
        self.scan_display = ScanDisplay(self.session_name)
        self._pool = ThreadPool(1)
        # self.start_time = 0
        # self.last_time = 0
        # self.stop_time = 0

    def update_counter_selection(self):
        self.counter_selection = self.scan_display.counters

    def get_selected_counters(self, counter_names):
        selection = []
        if not self.counter_selection:
            for cname in counter_names:
                if cname != "timer:epoch":
                    selection.append(cname)
        else:
            for cname in counter_names:
                if cname in self.counter_selection or cname == "timer:elapsed_time":
                    selection.append(cname)

        return selection

    def on_scan_new(self, scan_info):
        # self.start_time = time.time()
        # Skip other session
        if scan_info.get("session_name") != self.session_name:
            # print(f"{scan_info.get('session_name')} != {self.session_name}")
            return

        scan_type = scan_info.get("type")
        npoints = scan_info.get("npoints")

        # Skip bad scans
        if None in [scan_type, npoints]:
            # print("scan_type, npoints = ",scan_type, npoints)
            return

        # Skip secondary scans and warn user
        if self.scan_is_running:
            print(
                f"Warning: a new scan '{scan_info.get('node_name')}' has been started while scan '{self.scan_name}' is running.\nNew scan outputs will be ignored."
            )
            return
        else:
            self.scan_is_running = True
            self.scan_name = scan_info.get("node_name")
            self.update_counter_selection()

            # session_name = scan_info.get('session_name')             # ex: 'test_session'
            # user_name = scan_info.get('user_name')                   # ex: 'pguillou'
            # filename = scan_info.get('filename')                     # ex: '/mnt/c/tmp/test_session/data.h5'
            # node_name = scan_info.get('node_name')                   # ex: 'test_session:mnt:c:tmp:183_ascan'

            # start_time = scan_info.get('start_time')                 # ex: datetime.datetime(2019, 3, 18, 15, 28, 17, 83204)
            # start_time_str = scan_info.get('start_time_str')         # ex: 'Mon Mar 18 15:28:17 2019'
            # start_timestamp = scan_info.get('start_timestamp')       # ex: 1552919297.0832036

            # save = scan_info.get('save')                             # ex: True
            # sleep_time = scan_info.get('sleep_time')                 # ex: None

            # title = scan_info.get('title')                           # ex: 'ascan roby 0 10 10 0.01'
            # scan_type = scan_info.get('type')                        # ex:    ^
            # start = scan_info.get('start')                           # ex:             ^              = [0]
            # stop = scan_info.get('stop')                             # ex:                ^           = [10]
            # npoints = scan_info.get('npoints')                       # ex:                   ^        = 10
            # count_time = scan_info.get('count_time')                 # ex:                       ^    = 0.01

            # total_acq_time = scan_info.get('total_acq_time')         # ex: 0.1  ( = npoints * count_time )
            # scan_nb = scan_info.get('scan_nb')                       # ex: 183

            # positioners_dial = scan_info.get('positioners_dial')     # ex: {'bad': 0.0, 'calc_mot1': 20.0, 'roby': 20.0, ... }
            # positioners = scan_info.get('positioners')               # ex: {'bad': 0.0, 'calc_mot1': 20.0, 'roby': 10.0, ...}

            # estimation = scan_info.get('estimation')                 # ex: {'total_motion_time': 2.298404048112306, 'total_count_time': 0.1, 'total_time': 2.398404048112306}
            # acquisition_chain = scan_info.get('acquisition_chain')   # ex: {'axis': {'master': {'scalars': ['axis:roby'], 'spectra': [], 'images': []}, 'scalars': ['timer:elapsed_time', 'diode:diode'], 'spectra': [], 'images': []}}

            self.scan_steps_index = 1
            self.col_labels = ["#"]
            self._point_nb = 0

            master, channels = next(iter(scan_info["acquisition_chain"].items()))

            selected_counters = self.get_selected_counters(channels["scalars"])

            # remove epoch from channel_names
            ch_master_scalar_wo_epoch = []
            for cname in channels["master"]["scalars"]:
                if cname != "timer:epoch":
                    ch_master_scalar_wo_epoch.append(cname)

            self.channel_names = ch_master_scalar_wo_epoch + selected_counters

            # get the number of masters and counters unfiltered
            self.channels_number = len(channels["master"]["scalars"]) + len(
                channels["scalars"]
            )

            # BUILD THE LABEL COLUMN
            channel_labels = []
            # GET THE LIST OF MASTER CHANNELS SHORT NAMES
            for channel_name in ch_master_scalar_wo_epoch:
                channel_short_name = channels["master"]["display_names"][channel_name]
                channel_unit = channels["master"]["scalars_units"][channel_name]

                if channel_short_name == "elapsed_time":
                    self.col_labels.insert(1, f"dt[{channel_unit}]")
                else:
                    channel_labels.append(
                        channel_short_name
                        + (f"[{channel_unit}]" if channel_unit else "")
                    )

            # GET THE LIST OF SCALAR CHANNELS SHORT NAMES
            for channel_name in selected_counters:
                channel_short_name = channels["display_names"][channel_name]
                channel_unit = channels["scalars_units"][channel_name]

                if channel_short_name == "elapsed_time":
                    self.col_labels.insert(1, f"dt[{channel_unit}]")
                else:
                    channel_labels.append(
                        channel_short_name
                        + (f"[{channel_unit}]" if channel_unit else "")
                    )

            self.col_labels.extend(channel_labels)

            # GET THE LIST OF OTHER CHANNELS ('spectra' and 'images')
            other_channels = []
            for channel_name in channels["spectra"] + channels["images"]:
                # idx = channel_name.rfind(":") + 1
                # channel_short_name = channel_name[idx:]
                # other_channels.append(channel_short_name)
                other_channels.append(channel_name)

            if other_channels:
                not_shown_counters_str = (
                    "Activated counters not shown: %s\n" % ", ".join(other_channels)
                )
            else:
                not_shown_counters_str = ""

            if scan_type == "ct":
                header = not_shown_counters_str
            else:
                estimation = scan_info.get("estimation")
                if estimation:
                    total = datetime.timedelta(seconds=estimation["total_time"])
                    motion = datetime.timedelta(seconds=estimation["total_motion_time"])
                    count = datetime.timedelta(seconds=estimation["total_count_time"])
                    estimation_str = ", {0} (motion: {1}, count: {2})".format(
                        total, motion, count
                    )
                else:
                    estimation_str = ""

                col_lens = [max(len(x), self.DEFAULT_WIDTH) for x in self.col_labels]
                h_templ = [
                    "{{0:>{width}}}".format(width=col_len) for col_len in col_lens
                ]
                header = "  ".join(
                    [
                        templ.format(label)
                        for templ, label in zip(h_templ, self.col_labels)
                    ]
                )
                header = self.HEADER.format(
                    column_header=header,
                    estimation_str=estimation_str,
                    not_shown_counters_str=not_shown_counters_str,
                    **scan_info,
                )
                self.col_templ = [
                    "{{0: >{width}g}}".format(width=col_len) for col_len in col_lens
                ]

            print(header)

    def on_scan_new_child(self, scan_info, data_channel):
        pass

    @_post_in_pool
    def on_scan_data(self, data_dim, master_name, channel_info):

        if data_dim != "0d":
            return

        scan_info = channel_info["scan_info"]
        scan_type = scan_info.get("type")

        # Skip other session
        if scan_info.get("session_name") != self.session_name:
            return

        # Skip other scan
        if scan_info.get("node_name") != self.scan_name:
            return

        # Skip if missing channels
        if len(channel_info["data"]) != self.channels_number:
            return

        # Skip if partial data
        for channel_name in self.channel_names:
            if len(channel_info["data"][channel_name]) < self.scan_steps_index:
                return

        # Check if we receive more than one scan points (i.e. lines) per 'scan_data' event
        bsize = min(
            [
                len(channel_info["data"][channel_name])
                for channel_name in channel_info["data"]
            ]
        )

        for i in range(bsize - self.scan_steps_index + 1):
            # Get data for the current scan step
            values_dict = {}

            for channel_name in self.channel_names:
                values_dict[channel_name] = channel_info["data"][channel_name][
                    self.scan_steps_index - 1
                ]

            # Extract time data
            elapsed_time_col = []
            if "timer:elapsed_time" in values_dict:
                elapsed_time_col.append(values_dict.pop("timer:elapsed_time"))

            # Build data line
            values = elapsed_time_col + [
                values_dict[channel_name] for channel_name in values_dict
            ]

            # Format output line
            if scan_type == "ct":
                # ct is actually a timescan(npoints=1).
                norm_values = numpy.array(values) / scan_info["count_time"]
                col_len = max(map(len, self.col_labels)) + 2
                template = "{{label:>{0}}} = {{value: >12}} ({{norm: 12}}/s)".format(
                    col_len
                )
                lines = "\n".join(
                    [
                        template.format(label=label, value=v, norm=nv)
                        for label, v, nv in zip(
                            self.col_labels[1:], values, norm_values
                        )
                    ]
                )
                end_time_str = datetime.datetime.now().strftime("%a %b %d %H:%M:%S %Y")
                msg = "{0}\n\n{1}".format(end_time_str, lines)
                print(msg)
            else:
                values.insert(0, self._point_nb)
                self._point_nb += 1
                line = "  ".join(
                    [self.col_templ[i].format(v) for i, v in enumerate(values)]
                )

                print(line)

            self.scan_steps_index += 1

        # self.last_time = time.time()
        # print("dt_last = ",self.last_time -self.start_time)

    def on_scan_end(self, scan_info):

        # self.stop_time = time.time()
        # print("stop_time ", self.stop_time)
        # print("dt_stop = ",self.stop_time -self.start_time)

        if scan_info.get("session_name") != self.session_name:
            return

        if scan_info.get("node_name") != self.scan_name:
            return

        end = datetime.datetime.fromtimestamp(time.time())
        start = datetime.datetime.fromtimestamp(scan_info["start_timestamp"])
        dt = end - start

        msg = "\nTook {0}".format(dt)
        if "estimation" in scan_info:
            time_estimation = scan_info["estimation"]["total_time"]
            msg += " (estimation was for {0})".format(
                datetime.timedelta(seconds=time_estimation)
            )
        print(msg)

        print_full_line(
            " >>> PRESS F5 TO COME BACK TO THE SHELL PROMPT <<< ",
            deco="=",
            head="\n",
            tail="\n",
        )

        self.scan_is_running = False

    def reset_terminal(self):
        # Prevent user inputs
        fd = sys.stdin.fileno()
        new = termios.tcgetattr(fd)
        new[3] |= termios.ECHO
        termios.tcsetattr(fd, termios.TCSANOW, new)

    def start(self):

        # Prevent user to close the listener with Ctrl-C
        signal.signal(signal.SIGINT, catch_sigint)

        # Prevent user inputs if using a terminal
        fd = sys.stdin.fileno()
        try:
            new = termios.tcgetattr(fd)
            new[3] &= ~termios.ECHO
            termios.tcsetattr(fd, termios.TCSANOW, new)
        except termios.error:
            pass  # not in terminal (example in tests)
        else:
            # revert 'Prevent user inputs if using a terminal'
            atexit.register(self.reset_terminal)

        print_full_line(
            f" Bliss session '{self.session_name}': watching scans ",
            deco="=",
            head="",
            tail="\n",
        )

        # Start the watch, winter is coming...
        watch_session_scans(
            self.session_name,
            self.on_scan_new,
            self.on_scan_new_child,
            self.on_scan_data,
            self.on_scan_end,
            exit_read_fd=self.exit_read_fd,
        )


@contextlib.contextmanager
def _local_pb(scan, repl, task):
    # Shitty cyclic import
    # we have to purge this :-(
    from bliss.shell.cli import progressbar

    def stop():
        task.kill()

    repl.register_application_stopper(stop)
    try:
        real_motors = list()

        def on_motor_position_changed(position, signal=None, sender=None):
            labels = []
            for motor in real_motors:
                position = "{0:.03f}".format(motor.position)
                unit = motor.config.get("unit", default=None)
                if unit:
                    position += "[{0}]".format(unit)
                labels.append("{0}: {1}".format(motor.name, position))
            pb.bar.label = ", ".join(labels)
            pb.invalidate()

        scan_info = scan.scan_info
        master, channels = next(iter(scan_info["acquisition_chain"].items()))
        for channel_fullname in channels["master"]["scalars"]:
            channel_short_name = channels["master"]["display_names"][channel_fullname]
            try:
                motor = _find_obj(channel_short_name)
            except Exception:
                continue
            else:
                if isinstance(motor, Axis):
                    real_motors.append(motor)
                    dispatcher.connect(
                        on_motor_position_changed, signal="position", sender=motor
                    )
        if scan.scan_info.get("type") == "ct":

            class my_pb(progressbar.ProgressBar):
                def __call__(self, queue, **keys):
                    npoints = int(scan.scan_info.get("count_time", 1) // .1) or None
                    keys["total"] = npoints
                    self._ct_tick_task = None
                    if npoints:

                        def tick():
                            for i in range(npoints):
                                queue.put("-")
                                gevent.sleep(.1)

                        self._ct_tick_task = gevent.spawn(tick)
                    return super().__call__(queue, **keys)

                def __exit__(self, *args, **kwargs):
                    if self._ct_tick_task is not None:
                        self._ct_tick_task.kill()
                    super().__exit__(*args, **kwargs)

            with my_pb() as pb:
                yield pb
        else:
            with progressbar.ProgressBar() as pb:
                yield pb
    except KeyboardInterrupt:
        repl.stop_current_task(block=False, exception=KeyboardInterrupt)
    finally:
        repl.unregister_application_stopper(stop)
        for motor in real_motors:
            dispatcher.disconnect(
                on_motor_position_changed, signal="position", sender=motor
            )


class ScanEventHandler(ScanPrinter):
    def __init__(self, repl):

        self.repl = repl
        self.progress_task = None
        self._on_scan_data_values = None
        super().__init__()

    def on_scan_new(self, scan, scan_info):
        if self.progress_task:
            self.progress_task.kill()

        self._on_scan_data_values = None

        # display progressbar only in repl greenlet
        if self.repl.current_task != gevent.getcurrent():
            return

        started_event = gevent.event.Event()
        self.progress_task = gevent.spawn(self._progress_bar, scan, started_event)
        with gevent.Timeout(1.):
            started_event.wait()

    def on_scan_data(self, *args):
        self._on_scan_data_values = args

    def on_scan_end(self, scan_info):
        pass

    def _progress_bar(self, scan, started_event):

        try:
            npoints = scan.scan_info["npoints"]
        except KeyError:
            started_event.set()
            return  # nothing to do
        queue = gevent.queue.Queue()
        task = self.progress_task

        class Preset(ChainPreset):
            class Iter(ChainIterationPreset):
                def stop(self):
                    queue.put("+")

            def get_iterator(self, chain):
                while True:
                    yield Preset.Iter()

            def stop(self, chain):
                queue.put(StopIteration)
                task.join()

        preset = Preset()
        scan.acq_chain.add_preset(preset)
        started_event.set()
        try:
            with _local_pb(scan, self.repl, task) as pb:
                it = pb(queue, remove_when_done=True, total=npoints or None)
                pb.bar = it
                for i in it:
                    pass
        finally:
            if self._on_scan_data_values:
                self.on_scan_data_ct(*self._on_scan_data_values)
