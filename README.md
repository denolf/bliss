Bliss
======

[![build status](https://gitlab.esrf.fr/bliss/bliss/badges/master/build.svg)](http://bliss.gitlab-pages.esrf.fr/bliss)
[![coverage report](https://gitlab.esrf.fr/bliss/bliss/badges/master/coverage.svg)](http://bliss.gitlab-pages.esrf.fr/bliss/htmlcov)

The bliss control library.

Latest documentation from master can be found [here](http://bliss.gitlab-pages.esrf.fr/bliss/master)


In short
========

To update BLISS from source:
```
conda install --file ./requirements-conda.txt
pip install --no-deps -e .
```




